#!/bin/bash
if [ ! -e kadm5.acl ] ; then 
	touch kadm5.acl
fi
echo "$principal@VEHICLEFORGE.COM ac" > kadm5.acl
git add kadm5.acl
git commit -m "added kadm5.acl"
git push origin master
mv kadm5.acl /var/kerberos/krb5kdc/kadm5.acl
service krb5kdc start
service kadmin start
