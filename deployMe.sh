#!/bin/bash
yum update -y
yum install -y krb5-server krb5-libs krb5-auth-dialog
yum install -y git
/usr/sbin/kdb5_util create -s -P $kerberosPass
/usr/sbin/kadmin.local -q "addprinc -pw $inputPass $inputAdmin"
/usr/sbin/kadmin.local -q "addprinc -pw $idpRandomPass idp"
/usr/sbin/kadmin.local -q "addprinc -pw $adminRandomPass kadmin/kadmin"
/usr/sbin/kadmin.local -q "addprinc -pw $changepwRandomPass kadmin/changepw"
/usr/sbin/kadmin.local -q "ktadd -k idp.keytab idp"
git add idp.keytab
git commit -m "adding new keytab"
git push origin master
if [ ! -e kadm5.acl ] ; then 
	touch kadm5.acl
fi
echo "$principal@VEHICLEFORGE.COM ac" > kadm5.acl
git add kadm5.acl
git commit -m "added kadm5.acl"
git push origin master
mv kadm5.acl /var/kerberos/krb5kdc/kadm5.acl
service krb5kdc start
service kadmin start
